<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $fitter->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $fitter->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Fitters'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="fitters form large-9 medium-8 columns content">
    <?= $this->Form->create($fitter) ?>
    <fieldset>
        <legend><?= __('Edit Fitter') ?></legend>
        <?php
            echo $this->Form->control('name');
            echo $this->Form->control('cpf');
            echo $this->Form->control('email');
            echo $this->Form->control('d_e_l_e_t_e');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
