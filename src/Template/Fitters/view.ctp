<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Fitter'), ['action' => 'edit', $fitter->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Fitter'), ['action' => 'delete', $fitter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fitter->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Fitters'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Fitter'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="fitters view large-9 medium-8 columns content">
    <h3><?= h($fitter->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($fitter->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Cpf') ?></th>
            <td><?= h($fitter->cpf) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Email') ?></th>
            <td><?= h($fitter->email) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($fitter->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('D E L E T E') ?></th>
            <td><?= $this->Number->format($fitter->d_e_l_e_t_e) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($fitter->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($fitter->modified) ?></td>
        </tr>
    </table>
</div>
