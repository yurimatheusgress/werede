<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Fitter'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="fitters index large-9 medium-8 columns content">
    <h3><?= __('Fitters') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('name') ?></th>
                <th scope="col"><?= $this->Paginator->sort('cpf') ?></th>
                <th scope="col"><?= $this->Paginator->sort('email') ?></th>
                <th scope="col"><?= $this->Paginator->sort('d_e_l_e_t_e') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($fitters as $fitter): ?>
            <tr>
                <td><?= $this->Number->format($fitter->id) ?></td>
                <td><?= h($fitter->name) ?></td>
                <td><?= h($fitter->cpf) ?></td>
                <td><?= h($fitter->email) ?></td>
                <td><?= $this->Number->format($fitter->d_e_l_e_t_e) ?></td>
                <td><?= h($fitter->created) ?></td>
                <td><?= h($fitter->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $fitter->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $fitter->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $fitter->id], ['confirm' => __('Are you sure you want to delete # {0}?', $fitter->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
