<?php
/**
  * @var \App\View\AppView $this
  */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Network'), ['action' => 'edit', $network->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Network'), ['action' => 'delete', $network->id], ['confirm' => __('Are you sure you want to delete # {0}?', $network->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Networks'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Network'), ['action' => 'add']) ?> </li>
        <li><?= $this->Html->link(__('List Stores'), ['controller' => 'Stores', 'action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Store'), ['controller' => 'Stores', 'action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="networks view large-9 medium-8 columns content">
    <h3><?= h($network->name) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Name') ?></th>
            <td><?= h($network->name) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($network->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('D E L E T E') ?></th>
            <td><?= $this->Number->format($network->d_e_l_e_t_e) ?></td>
        </tr>
    </table>
    <div class="related">
        <h4><?= __('Related Stores') ?></h4>
        <?php if (!empty($network->stores)): ?>
        <table cellpadding="0" cellspacing="0">
            <tr>
                <th scope="col"><?= __('Id') ?></th>
                <th scope="col"><?= __('Name') ?></th>
                <th scope="col"><?= __('Cnpj') ?></th>
                <th scope="col"><?= __('Network Id') ?></th>
                <th scope="col"><?= __('Created') ?></th>
                <th scope="col"><?= __('D E L E T E') ?></th>
                <th scope="col"><?= __('Modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
            <?php foreach ($network->stores as $stores): ?>
            <tr>
                <td><?= h($stores->id) ?></td>
                <td><?= h($stores->name) ?></td>
                <td><?= h($stores->cnpj) ?></td>
                <td><?= h($stores->network_id) ?></td>
                <td><?= h($stores->created) ?></td>
                <td><?= h($stores->d_e_l_e_t_e) ?></td>
                <td><?= h($stores->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['controller' => 'Stores', 'action' => 'view', $stores->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['controller' => 'Stores', 'action' => 'edit', $stores->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['controller' => 'Stores', 'action' => 'delete', $stores->id], ['confirm' => __('Are you sure you want to delete # {0}?', $stores->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </table>
        <?php endif; ?>
    </div>
</div>
