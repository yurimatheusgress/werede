<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Fitters Controller
 *
 * @property \App\Model\Table\FittersTable $Fitters
 */
class FittersController extends AppController
{

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        $fitters = $this->paginate($this->Fitters);

        $this->set(compact('fitters'));
        $this->set('_serialize', ['fitters']);
    }

    /**
     * View method
     *
     * @param string|null $id Fitter id.
     * @return \Cake\Network\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $fitter = $this->Fitters->get($id, [
            'contain' => []
        ]);

        $this->set('fitter', $fitter);
        $this->set('_serialize', ['fitter']);
    }

    /**
     * Add method
     *
     * @return \Cake\Network\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $fitter = $this->Fitters->newEntity();
        if ($this->request->is('post')) {
            $fitter = $this->Fitters->patchEntity($fitter, $this->request->getData());
            if ($this->Fitters->save($fitter)) {
                $this->Flash->success(__('The fitter has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fitter could not be saved. Please, try again.'));
        }
        $this->set(compact('fitter'));
        $this->set('_serialize', ['fitter']);
    }

    /**
     * Edit method
     *
     * @param string|null $id Fitter id.
     * @return \Cake\Network\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Network\Exception\NotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $fitter = $this->Fitters->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $fitter = $this->Fitters->patchEntity($fitter, $this->request->getData());
            if ($this->Fitters->save($fitter)) {
                $this->Flash->success(__('The fitter has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The fitter could not be saved. Please, try again.'));
        }
        $this->set(compact('fitter'));
        $this->set('_serialize', ['fitter']);
    }

    /**
     * Delete method
     *
     * @param string|null $id Fitter id.
     * @return \Cake\Network\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $fitter = $this->Fitters->get($id);
        if ($this->Fitters->delete($fitter)) {
            $this->Flash->success(__('The fitter has been deleted.'));
        } else {
            $this->Flash->error(__('The fitter could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
