<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * StoresFixture
 *
 */
class StoresFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => '19', 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => '200', 'null' => false, 'default' => null, 'collate' => 'Latin1_General_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'cnpj' => ['type' => 'string', 'fixed' => true, 'length' => '28', 'null' => false, 'default' => null, 'collate' => 'Latin1_General_CI_AS', 'precision' => null, 'comment' => null],
        'network_id' => ['type' => 'biginteger', 'length' => '19', 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null],
        'd_e_l_e_t_e' => ['type' => 'integer', 'length' => '3', 'null' => false, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        '_indexes' => [
            'IX_stores_cnpj' => ['type' => 'index', 'columns' => ['cnpj'], 'length' => []],
            'IX_stores_name' => ['type' => 'index', 'columns' => ['name'], 'length' => []],
            'IX_stores_deleted' => ['type' => 'index', 'columns' => ['d_e_l_e_t_e'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
            'FK_stores_networks' => ['type' => 'foreign', 'columns' => ['network_id'], 'references' => ['networks', 'id'], 'update' => 'cascade', 'delete' => 'cascade', 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'cnpj' => 'Lorem ipsum dolor sit amet',
            'network_id' => 1,
            'created' => 1492046618,
            'd_e_l_e_t_e' => 1,
            'modified' => 1492046618
        ],
    ];
}
