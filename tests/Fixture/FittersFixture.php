<?php
namespace App\Test\Fixture;

use Cake\TestSuite\Fixture\TestFixture;

/**
 * FittersFixture
 *
 */
class FittersFixture extends TestFixture
{

    /**
     * Fields
     *
     * @var array
     */
    // @codingStandardsIgnoreStart
    public $fields = [
        'id' => ['type' => 'biginteger', 'length' => '19', 'autoIncrement' => true, 'null' => false, 'default' => null, 'precision' => null, 'comment' => null, 'unsigned' => null],
        'name' => ['type' => 'string', 'length' => '300', 'null' => false, 'default' => null, 'collate' => 'Latin1_General_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'cpf' => ['type' => 'string', 'fixed' => true, 'length' => '22', 'null' => false, 'default' => null, 'collate' => 'Latin1_General_CI_AS', 'precision' => null, 'comment' => null],
        'email' => ['type' => 'string', 'length' => '300', 'null' => false, 'default' => null, 'collate' => 'Latin1_General_CI_AS', 'precision' => null, 'comment' => null, 'fixed' => null],
        'd_e_l_e_t_e' => ['type' => 'integer', 'length' => '3', 'null' => false, 'default' => '0', 'precision' => null, 'comment' => null, 'unsigned' => null, 'autoIncrement' => null],
        'created' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        'modified' => ['type' => 'timestamp', 'length' => null, 'null' => true, 'default' => null, 'precision' => null, 'comment' => null],
        '_indexes' => [
            'IX_fitters_cpf' => ['type' => 'index', 'columns' => ['cpf'], 'length' => []],
            'IX_fitters_name' => ['type' => 'index', 'columns' => ['name'], 'length' => []],
            'IX_fitters_email' => ['type' => 'index', 'columns' => ['email'], 'length' => []],
        ],
        '_constraints' => [
            'primary' => ['type' => 'primary', 'columns' => ['id'], 'length' => []],
        ],
    ];
    // @codingStandardsIgnoreEnd

    /**
     * Records
     *
     * @var array
     */
    public $records = [
        [
            'id' => 1,
            'name' => 'Lorem ipsum dolor sit amet',
            'cpf' => 'Lorem ipsum dolor si',
            'email' => 'Lorem ipsum dolor sit amet',
            'd_e_l_e_t_e' => 1,
            'created' => 1492046632,
            'modified' => 1492046632
        ],
    ];
}
